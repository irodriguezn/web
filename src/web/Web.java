/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author nachorod
 */
public class Web {
    private String url;//"http://cara.hol.es/beatles/";
    private String ruta;//"./src/web/beatles/index.html";
    private String objeto;
    
    public Web(String url, String ruta, String objeto) {
        this.url=url;
        this.ruta=ruta;
        this.objeto=objeto;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }
    
    public void bajaArchivo() {
        try {
            URL dirWeb = new URL(url+objeto);
            URLConnection urlCon = dirWeb.openConnection();
            // acceso al contenido web
            InputStream is = urlCon.getInputStream();

            // Fichero en el que queremos guardar el contenido
            FileOutputStream fos = new FileOutputStream(ruta+objeto);

            // buffer para ir leyendo.
            byte [] array = new byte[1000];

            // Primera lectura y bucle hasta el final
            int leido = is.read(array);
            while (leido > 0) {
                fos.write(array,0,leido);
                leido=is.read(array);
            }

            // Cierre de conexion y fichero.
            is.close();
            fos.close();
            
        } catch (Exception e) {

        }   
    }

}
    
    
