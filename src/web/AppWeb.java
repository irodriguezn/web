package web;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 *
 * @author nachorod
 */
public class AppWeb {
    public static void main(String[] args) throws Exception {
        String url="http://cara.hol.es/beatles/";
        String objeto="index.html";
        String ruta="./src/web/beatles/";
        Web www=new Web(url, ruta, objeto);
        www.bajaArchivo();
        String rutaImagen="";
        FileReader fr=new FileReader(ruta+objeto);
        BufferedReader br=new BufferedReader(fr);
        String linea=br.readLine();
        while (linea!=null) {
            rutaImagen=buscaImagen(linea);
            if (!rutaImagen.equals("")) {
                objeto=rutaImagen;
                www=new Web(url, ruta, objeto);
                www.bajaArchivo();
            }
            linea=br.readLine();
        }
    }
    
    static String buscaImagen(String linea) {
        String flagImagen="<img src=";
        String ruta="";
        int pos=linea.indexOf(flagImagen);
        if (pos>0) {
            int posI=linea.indexOf("\"", pos)+1;
            int posF=linea.indexOf("\"", posI);
            ruta=linea.substring(posI, posF);
        }
        return ruta;
    }
}
